-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2016 at 12:53 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(30) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('fahad', '81dc9bdb52d04dc20036dbd8313ed055'),
('huraon', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `booklist`
--

CREATE TABLE `booklist` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `edition` varchar(20) NOT NULL,
  `category` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `location` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `cover` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booklist`
--

INSERT INTO `booklist` (`id`, `name`, `author`, `edition`, `category`, `type`, `location`, `amount`, `cover`) VALUES
(13, 'The C Programming Language ', 'Brian W. Kernighan and Dennis M. Ritchie', '2nd', 'Programming', '', 'Shelf 1', 24, '1470204021download2.jpg'),
(14, 'Programming in C ', 'Stephen Kochan', '3rd', 'Programming', '', 'Shelf 1', 30, '1470204038download.png'),
(15, 'How to Program', 'Paul Deitel & Harvey M. Deite', '6th', 'Programming', '', 'Shelf 1', 0, '1470205336download3.jpg'),
(16, 'C Primer Plus - ', 'Stephen Prata', '6th', 'Programming', '', 'Shelf 1', 0, '1470205897download4.jpg'),
(17, 'C - Traps and Pitfalls ', 'Andrew R. Koenig (Bell Labs)', '2nd', 'Programming', '', 'Shelf 1', 30, '1470206101download5.jpg'),
(18, 'Programming: Principles and Practice Using C++ ', 'Bjarne Stroustrup', '2nd', 'Programming', '', 'Shelf 1', 0, '1470206631download6.jpg'),
(19, 'C++ Primer * ', 'Stanley Lippman, JosÃ©e Lajoie, and Barbara E. Moo', '5th', 'Programming', '', 'Shelf 1', 20, '1470206745download7.jpg'),
(20, 'Exceptional C++ Style ', 'Herb Sutter', '2nd', 'Programming', '', 'Shelf 1', 0, '1470206899download8.jpg'),
(21, 'Java in a Nutshell', 'Benjamin,J.Evans and David Flangan', '6th Edition', 'Programming', '', 'Shelf 1', 30, '1470207474download9.jpg'),
(22, 'Hooked on Java: Creating hot Web sites with Java applets', 'Van Hoff, Arthur', '1st', 'Programming', '', 'Shelf 1', 30, '147020779341jzgZmIlAL.jpg'),
(23, 'The Truth About HTML5', 'Luke Stevens', '1st', 'Programming', '', 'Shelf 1', 0, '1470208027file.jpeg'),
(24, ' The Essential Guide to CSS and HTML Web Design', 'Craig Grannell', '1st', 'Programming', '', 'Shelf 1', 20, '1470208200file (1).jpeg'),
(25, ' Smashing CSS', 'Eric Meyer', '1st', 'Programming', '', 'Shelf 1', 15, '1470208367file (2).jpeg'),
(26, 'Core HTML5 Canvas', 'David Geary', '1st', 'Programming', '', 'Shelf 1', 0, '1470208533file (3).jpeg'),
(27, ' Styling with CSS', 'Charles Wyke Smith', '3rd', 'Programming', '', 'Shelf 1', 17, '1470208697file (4).jpeg'),
(28, 'PHP & MySQL: Novice to Ninja', 'Kevin Yank', '5th', 'Programming', '', 'Shelf 1', 21, '1470208918download10.jpg'),
(29, 'PHP for the Web: Visual QuickStart Guide ', 'Larry Ullman', '4th', 'Programming', '', 'Shelf 1', 0, '1470209033download11.jpg'),
(30, 'Learning PHP, MySQL & JavaScript: With jQuery, CSS & HTML5 ', 'Robin Nixon', '1st', 'Programming', '', 'Shelf 1', 0, '1470209155download12.jpg'),
(31, 'PHP Objects, Patterns, and Practice ', 'Matt Zandstra', '4th', 'Programming', '', 'Shelf 1', 19, '1470209264download13.jpg'),
(32, 'Advanced PHP Programming ', 'George Schlossnagle', '2nd', 'Programming', '', 'Shelf 1', 22, '1470209426download14.jpg'),
(33, ' Fundamentals of Physics ', 'David Halliday, Robert Resnick and Jearl Walker', '9th', 'Physics', '', 'Shelf 2', 14, '1470244567download15.jpg'),
(34, ' Physics for Scientists and Engineers with Modern Physics ', 'Douglas C. Giancoli', '2nd', 'Physics', '', 'Shelf 2', 13, '1470244722download16.jpg'),
(35, ' Physics for Scientists and Engineers: A Strategic Approach', 'Randall D. Knight', '3rd', 'Physics', '', 'Shelf 2', 11, '1470244854download17.jpg'),
(36, ' The Feynman Lectures on Physics ', 'Richard Feynman', '1st', 'Physics', '', 'Shelf 2', 0, '1470244994download18.jpg'),
(37, 'University Physics with Modern Physics ', 'Young, Freedman & Lewis Ford', '13', 'Physics', '', 'Shelf 2', 0, '1470245116download19.jpg'),
(38, 'The strange theory of light and matter .', 'Richard Feynman', '1st', 'Physics', '', 'Shelf 2', 0, '1470245335download20.jpg'),
(39, 'The Namesake (Paperback)  ', 'Jhumpa Lahiri', '', 'Novel', '', 'Shelf 3', 5, '1470247703download21.jpg'),
(40, 'à¦šà¦¾à¦à¦¦à§‡à¦° à¦ªà¦¾à¦¹à¦¾à¦¡à¦¼ ', 'Bibhutibhushan Bandyopadhyay', '', 'Novel', '', 'Shelf 3', 2, '1470247849download22.jpg'),
(41, 'The Home and the World ', 'Rabindranath Tagore', '', 'Novel', '', 'Shelf 3', 0, '1470248052download23.jpg'),
(42, 'à¦¦à§à¦°à§à¦—à§‡à¦¶à¦¨à¦¨à§à¦¦à¦¿à¦¨à§€', 'à¦¬à¦™à§à¦•à¦¿à¦®à¦šà¦¨à§à¦¦à§à¦° à¦šà¦Ÿà§à¦Ÿà§‹à¦ªà¦¾à¦§à§à¦¯à¦¾à¦¯à¦¼', '', 'Novel', '', 'Shelf 3', 3, '1470248270images.jpg'),
(43, 'à¦•à¦²à¦•à¦¾à¦¤à¦¾à¦¯à¦¼ à¦«à§‡à¦²à§à¦¦à¦¾ ', 'Satyajit Ray', '', 'Novel', '', 'Shelf 3', 2, '1470248678download24.jpg'),
(44, 'Aronno ', 'Humayun Ahmed', '', 'Novel', '', 'Shelf 3', 0, '1470248798Arono by Humayun Ahmed.jpg'),
(45, 'Misir Alir Amimangsito Rahasya', 'Humayun Ahmed', '', 'Novel', '', 'Shelf 3', 15, '1470248959download25.jpg'),
(46, '.Nishad', 'Humayun Ahmed', '', 'Novel', '', 'Shelf 3', 30, '1470249069download26.jpg'),
(47, 'Solaris ', 'Stanislaw Lem', '', 'Science Fiction', '', 'Shelf 4', 0, '1470249405download27.jpg'),
(48, 'The Secret of Sinharat', 'Leigh Brackett', '', 'Science Fiction', '', 'Shelf 4', 0, '1470249541download28.jpg'),
(49, ' The Star King ', 'Jack Vance', '', 'Science Fiction', '', 'Shelf 4', 3, '147024960136215-v1-185x.JPG'),
(50, 'The Zero Stone', 'Andre Norton', '', 'Science Fiction', '', 'Shelf 4', 0, '147024974836216-v1-185x.JPG'),
(51, 'The Left Hand of Darkness ', 'Ursula K. Le Guin', '', 'Science Fiction', '', 'Shelf 4', 0, '147024984836217-v1-185x (1).JPG'),
(52, 'Roadside Picnic', 'Arkady and Boris Strugatsky', '', 'Science Fiction', '', 'Shelf 4', 7, '147024992236218-v1-185x.JPG'),
(53, ' Create Dynamic Web Pages Using PHP and MySQL (Practical Solutions Series)', 'David Tansley.Addison Wesley', '1st', 'Reference Book', '', 'Shelf 5', 0, '1470282783download29.jpg'),
(54, 'C Programming: A Modern Approach ', 'K. N. King', '2nd', 'Reference Book', '', 'Shelf 5', 0, '1470282923tumblr_inline_nu6tthHaPk1tbt8rv_400.jpg'),
(55, 'A Book on C ', 'Al Kelley and Ira Pohl', '4th', 'Reference Book', '', 'Shelf 5', 0, '1470283031file (5).jpeg'),
(56, 'PHP Bible', 'Converse.John Wiley & Sons Inc', '2nd', 'Reference Book', '', 'Shelf 5', 0, '1470283259download30.jpg'),
(57, ' Learn C The Hard Way ', 'Zed Shaw', '1st', 'Reference Book', '', 'Shelf 5', 0, '1470283405file (6).jpeg'),
(58, 'The C Book ', 'Mike Banahan, Declan Brady and Mark Doran', '2nd', 'Reference Book', '', 'Shelf 5', 0, '1470283613tumblr_inline_nu6tvrv6LP1tbt8rv_400.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `borrow`
--

CREATE TABLE `borrow` (
  `borrow_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `borrow_date` date NOT NULL,
  `due_date` date NOT NULL,
  `return_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `borrow`
--

INSERT INTO `borrow` (`borrow_id`, `book_id`, `student_id`, `borrow_date`, `due_date`, `return_date`) VALUES
(2, 1, 0, '2016-07-30', '0000-00-00', '0000-00-00'),
(3, 1, 0, '2016-07-30', '0000-00-00', '0000-00-00'),
(4, 1, 0, '2016-07-30', '0000-00-00', '0000-00-00'),
(10, 1, 0, '2016-08-01', '0000-00-00', '0000-00-00'),
(11, 1, 0, '2016-08-01', '0000-00-00', '0000-00-00'),
(12, 13, 0, '2016-08-04', '2016-08-11', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `requested_books`
--

CREATE TABLE `requested_books` (
  `request_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `dept` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `dept`, `password`) VALUES
(11, 'Nasrin', 'Computer science', '81dc9bdb52d04dc20036dbd8313ed055'),
(19, 'Aharna Sen', 'Computer science', '81dc9bdb52d04dc20036dbd8313ed055'),
(51, 'hello', '284fcfb183d1919532b3c7a6dba33873', '81dc9bdb52d04dc20036dbd8313ed055'),
(52, 'fahad', 'Computer science', '81dc9bdb52d04dc20036dbd8313ed055');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `booklist`
--
ALTER TABLE `booklist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `borrow`
--
ALTER TABLE `borrow`
  ADD PRIMARY KEY (`borrow_id`);

--
-- Indexes for table `requested_books`
--
ALTER TABLE `requested_books`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booklist`
--
ALTER TABLE `booklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `borrow`
--
ALTER TABLE `borrow`
  MODIFY `borrow_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `requested_books`
--
ALTER TABLE `requested_books`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
