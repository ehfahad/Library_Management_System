<?php

session_start();

include_once('vendor/autoload.php');

use App\Library\Library;
use App\User\Auth;
use App\Utility\Utility;
use App\Message\Message;

$auth = new Auth();


//if (array_key_exists("itemPerPage", $_SESSION)) {
//    if (array_key_exists("itemPerPage", $_GET))
//        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
//} else
//    $_SESSION['itemPerPage'] = 9;

$itemPerPage = 9;

$obj = new Library();
$totalItem = $obj->count();
//echo $totalItem;
$totalPage = ceil($totalItem / $itemPerPage);

if (array_key_exists("pageNumber", $_GET))
    $pageNumber = $_GET['pageNumber'];
else
    $pageNumber = 1;

$pagination = "";
for ($count = 1; $count <= $totalPage; $count++) {
    $class = ($pageNumber == $count) ? "active" : "";
    $pagination .= "<li class='$class'><a href='index.php?pageNumber=$count'>$count</a></li>";
}


$pageStartFrom = $itemPerPage * ($pageNumber - 1);

//$allInfo = $obj->paginator($pageStartFrom,$itemPerPage);


if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) {
    $allInfo = $obj->paginator($pageStartFrom, $itemPerPage);
}
if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET') && isset($_GET['category']))  {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
}
if ((strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) && isset($_GET['search'])) {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
}

$availableTitle=$obj->getAllSearchData();
$comma_separated= '"'.implode('","',$availableTitle).'"';
//
//$availableDescription=$obj->getAllDescription();
//$comma_separated2= '"'.implode('","',$availableDescription).'"';
////Utility::dd($comma_separated);

?>


<!DOCTYPE html>
<html>
<head>
    <style>
        .bottommargin{
            margin-bottom: 10px;
        }
        footer{
            background-color:#2c3742;
            box-shadow:inset 0px 0px 3px #111;
            color:#fff;
            font-size:14px;
            line-height:25px;
            padding:10px 0px 10px 0px;
            bottom: 0px;
        }

        footer a{
            color:#eee;
            text-decoration:none;
            border-bottom:1px dotted #888;
        }

        footer a:hover{
            color:#aaa;
            text-decoration:none;
            border:0px;
        }

        footer hr{
            margin-top: 10px;
            margin-bottom: 10px;
            border-top: #000 1px solid;
            border-bottom: #212121 1px solid;
        }

        footer .copy{
            font-size:13px;
            margin:15px 0px;
        }
        .leftpad5 {

            margin-right: 5px;
        }



    </style>
    <title>Library Management System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="Resources/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <link href="Resources/bootstrap/css/style3.css" rel="stylesheet" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <script src="https://code.jquery.com/jquery.js"></script>

    <!--  For autocomplete  -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

</head>
<body>

<div style="min-height: 810px" class="container-fluid background">


    <nav class="row navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a class="active" href="index.php" >Home</a></li>
                    <li><a href="index.php?category=<?php echo "Reference Book" ?>" >Reference Book</a></li>
                    <li><a href="index.php?category=<?php echo "Science Fiction" ?>" >Science Fiction</a></li>
                    <li><a href="index.php?category=<?php echo "Novel" ?>" >Novel</a></li>
                    <li><a href="index.php?category=<?php echo "Physics" ?>" >Physics</a></li>
                    <li><a href="index.php?category=<?php echo "Programming" ?>" >Programming</a></li>
                </ul>



                <div class="nav rightmaring navbar-nav navbar-right">
                    <li style="margin-top: 8px " class="">
                        <form class="form-inline" action="index.php" method="get">
                            <input  size="18" id="search"  class="form-control form-inline" placeholder="Search..." type="text" name="search">
                            <input type="submit" value="search" class="form-inline btn-success btn" >
                        </form>
                    </li>
                    <li>
                        <div class="col-sm-4 leftpad5 ">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="" class="dropdown-toggle " data-toggle="dropdown">LogIn/SignUp <b
                                            class="caret"></b></a>
                                    <ul class="dropdown-menu animated fadeInUp">
                                        <li><a href="views/user_login_signup.php">Login as Student</a></li>
                                        <li><a href="views/Admin/index.php">Login as Admin</a></li>
                                    </ul>
                                </li>
                            </ul>


                        </div>
                    </li>


                </div>
            </div>



        </div>
    </nav>


    <div class="row slidemargin">

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="Resources/Images/slide_image_1.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>পলান সরকার</h3>
                        <p>বাংলাদেশের বাতিঘর বিস্তীর্ন জনপদ আলোকিত করার দায়িত্ব নিয়েছেন তিনি একাই</p>
                    </div>
                </div>

                <div class="item">
                    <img src="Resources/Images/slide_image_2.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>হুমায়ূন আজাদ</h3>
                        <p>একজন প্রথাবিরোধী লেখক . মৌলবাদী শক্তির বিরুদ্ধে তিনি ছিলেন এক লড়াকু যোদ্ধা</p>
                    </div>
                </div>

                <div class="item">

                    <img src="Resources/Images/slide_image_3.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>হুমায়ূন আহমেদ</h3>
                        <p>বাংলা সাহিত্যের অন্যতম জনপ্রিয় লেখক। হিমু, মিসির আলীর মত চরিত্র সৃষ্টি করেছেন তিনি</p>
                    </div>
                </div>

                <div class="item">
                    <img src="Resources/Images/slide_image_4.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>Ahmed Chafa</h3>
                        <p>Most prominent writer.</p>
                    </div>
                </div>


            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

<div class="container">
    <div class="row">
        <div>
            <u><h2 class="text-center"><font color="#00008b">Library Management System</font> </h2></u>
        </div>


        <?php if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])) : ?>
        <div class="alert-info" id="message">
            <h4><center><?php echo Message::message() ?></center></h4>
        </div>
        <?php endif; ?>

        <?php foreach ($allInfo as $info) { ?>
            <div class="col-sm-4 bottommargin ">
                <div class="imgWrap">
                    <img src="Resources/Images/<?php echo $info->cover ?>" width="270px" height="421px" alt="polaroid"/><br>
                    <div class="imgDescription text-center">
                        <h3><?php echo $info->name ?> </h3>
                        <h4><?php echo $info->author ?></h4>
                    </div>
                </div>
            </div>
        <?php } ?>


    </div>
    </div>
</div>






<?php if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET') && empty($_GET['search']) && empty($_GET['category'])) { ?>
    <ul class="pagination">
        <?php if ($pageNumber > 1): ?> <li><a href='index.php?pageNumber=<?php echo $pageNumber - 1 ?>'> Prev </a></li> <?php endif; ?>
        <?php echo $pagination; ?>
        <?php if ($pageNumber < $totalPage): ?> <li><a href='index.php?pageNumber=<?php echo $pageNumber + 1 ?>'> Next </a></li> <?php endif; ?>
    </ul>
<?php } ?>


<footer>
    <div  class="container">

        <div class="copy text-center">
            Copyright 2016 <b>Core i5</b>
        </div>

    </div>
</footer>

<script>
    $('#message').show().delay(2000).fadeOut();


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>
<script>
    $( function() {
        var availableTags = [
            <?php echo $comma_separated ?>
        ];
        $( "#search" ).autocomplete({
            source: availableTags
        });
    } );
</script>


</body>
</html>
