<?php

namespace App\Admin;

//session_start();

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Auth extends DB
{
    public $id = "";
    public $username = "";
    public $password = "";

    public function __construct()
    {
        parent::__construct();
    }

    public function prepare($data = Array())
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = md5($data['password']);
        }
        return $this;

    }

    public function is_exist()
    {
        $query = "SELECT * FROM `admin` WHERE `username`='" . $this->username . "'";

        $result = mysqli_query($this->conn, $query);
        //$row= mysqli_fetch_assoc($result);
        if (mysqli_num_rows($result) > 0) {
            return TRUE;
        }
        else {
            return FALSE;
        }

    }

    public function is_registered()
    {
        $query = "SELECT * FROM `admin` WHERE `username`='" . $this->username . "' AND `password`='" . $this->password . "'";
        $result = mysqli_query($this->conn, $query);
        //$row= mysqli_fetch_assoc($result);
        if (mysqli_num_rows($result) > 0) {
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    public function logged_in()
    {
        if ((array_key_exists('admin_name', $_SESSION)) && (!empty($_SESSION['admin_name']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function log_out()
    {
        $_SESSION['admin_name'] = "";
        return TRUE;
    }
    
    
    public function deleteAdmin(){
        $query = "DELETE FROM `admin` WHERE `username`= '".$this->username."' ";
        //Utility::dd($query);
        $result = mysqli_query($this->conn, $query);
        if(mysqli_fetch_object($result)){
            Message::message("Deleted successfully");
        }
        else
            Message::message("Error Occured");
    }
}