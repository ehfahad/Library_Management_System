<?php
namespace App\Admin;
use App\Message\Message;
use App\Utility\Utility;
include_once('../../vendor/autoload.php');

use App\Model\Database as DB;

class User extends DB{
    public $name="";
    public $email="";
    public $password="";
    public $dept = "";
    public $id="";

    public function __construct()
    {
        parent::__construct();
    }

    public function prepare($data=array()){
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('dept',$data)){
            $this->dept=$data['dept'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }

        return $this;


    }

    public function store(){
        $query = "INSERT INTO `lms`.`student` (`id`, `name`, `dept`, `password`) VALUES ('".$this->id."','".$this->name."', '".$this->dept."', '".$this->password."')";
        $result= mysqli_query($this->conn,$query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("../../views/user_login_signup.php");
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Fail!</strong> Data has not been stored successfully.
                </div>");
            Utility::redirect("../../views/user_login_signup.php");
        }

    }




}
