<?php

namespace App\Library;

use App\Message\Message;
use App\Utility\Utility;

class Library{
    public $id = "";
    public $user_id ="";
    public $student_id = "";
    public $book_id="";
    public $request_id="";
    public $searchByCategory = "";
    public $search = "";
    public $name="";
    public $author="";
    public $edition="";
    public $type="";
    public $location="";
    public $is_available="";
    public $conn;
    public $amount="";
    public $cover="";
    public $borrow_id= "";
    public $borrow_date="";
    public $password = "";

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "lms");
    }

    public function prepare($data)
    {
        if (array_key_exists('id', $data))
            $this->id = $data['id'];
        if (array_key_exists('user_id', $data))
            $this->user_id = $data['user_id'];
        if (array_key_exists('student_id', $data))
            $this->student_id = $data['student_id'];
        if (array_key_exists('book_id', $data))
            $this->book_id = $data['book_id'];
        if (array_key_exists('request_id', $data))
            $this->request_id = $data['request_id'];
        if (array_key_exists("category", $data)) {
            $this->searchByCategory = $data['category'];
        }
        if (array_key_exists("search", $data)) {
            $this->search = $data['search'];
        }
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("password", $data))
            $this->password = md5($data['password']);
        if (array_key_exists("author", $data)) {
            $this->author = $data['author'];
        }
        if (array_key_exists("location", $data)) {
            $this->location = $data['location'];
        }
        if (array_key_exists("edition", $data)) {
            $this->edition = $data['edition'];
        }
        if (array_key_exists("amount", $data)) {
            $this->amount = $data['amount'];
        }
        if (array_key_exists("cover", $data)) {
            $this->cover = $data['cover'];
        }
        if (array_key_exists("borrow_date", $data)) {
            $this->borrow_date= $data['borrow_date'];
        }
        if (array_key_exists("borrow_id", $data)) {
            $this->borrow_id= $data['borrow_id'];
        }

        return $this;
    }
    public function store(){
        $query="INSERT INTO `lms`.`booklist` (`name`, `author`, `edition`, `category`, `location`, `amount`, `cover`) VALUES ('".$this->name."', '".$this->author."', '".$this->edition."', '".$this->searchByCategory."', '".$this->location."', '".$this->amount."', '".$this->cover."')";
        //Utility::dd($query);
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div>
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }


    public function index()
    {
        $whereClause= " 1=1 ";
        if(!empty($this->searchByCategory)){
            $whereClause.=" AND  category LIKE '%".$this->searchByCategory."%'";
        }
        if(!empty($this->search)){
            $whereClause.=" AND  name LIKE '%".$this->search."%' OR author LIKE '%".$this->search."%'";
        }


        $_allbook = array();
        $query = "SELECT * FROM `booklist` WHERE ".$whereClause;
        //Utility::dd($query);
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_object($result)) {
            $_allbook[] = $row;
        }
        return $_allbook;
    }
    public function view(){
        $query="SELECT * FROM `lms`.`booklist` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

//
//    public function issued_book($startFrom=0,$limit=5){
//
//        $_allbook = array();
//        $query="SELECT * FROM `borrow` LIMIT ".$startFrom.",".$limit;
//       // Utility::d($query);
//        $result = mysqli_query($this->conn, $query);
//
//        while ($row = mysqli_fetch_object($result)) {
//            $_allbook[] = $row;
//        }
//        return $_allbook;
//    }


    public function issued_books(){

        $_allbook = array();
        $query="SELECT bl.name as bname,bl.edition,s.dept,b.book_id,s.id,s.name as sname,b.borrow_date,b.due_date
FROM student s
LEFT JOIN borrow b
ON s.id=b.student_id
LEFT JOIN booklist bl
ON b.book_id=bl.id";
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_object($result)) {
            $_allbook[] = $row;
        }
        //Utility::d($_allbook);
        return $_allbook;
    }


    public function is_issued(){
        $_allbook = array();
        $query="SELECT bl.id,s.id
FROM student s
LEFT JOIN borrow b
ON s.id=b.student_id
LEFT JOIN booklist bl
ON b.book_id=bl.id WHERE bl.id= '".$this->book_id."' AND s.id='".$this->student_id."'";

        $result = mysqli_query($this->conn, $query);
        $row=mysqli_num_rows($result);
        if($row>0)
            return true;
        else
            return false;
    }
    

    public function update()
    {
        if(!empty($this->cover))
            $query = "UPDATE `lms`.`booklist` SET `name`='".$this->name."',`author`='".$this->author."',`edition`='".$this->edition."',`category`='".$this->searchByCategory."',`type`='".$this->type."',`location`='".$this->location."',`amount`='".$this->amount."',`cover`='".$this->cover."' WHERE `booklist`.`id` = ".$this->id;

        else
            $query = "UPDATE `lms`.`booklist` SET `name`='".$this->name."',`author`='".$this->author."',`edition`='".$this->edition."',`category`='".$this->searchByCategory."',`type`='".$this->type."',`location`='".$this->location."',`amount`='".$this->amount."' WHERE `booklist`.`id` = ".$this->id;

        //Utility::dd($query);
        $result=mysqli_query($this->conn,$query);
        //echo $result;
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }


    public function delete(){
        $query="DELETE FROM `lms`.`booklist`WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Sorry!</strong> Data has not been deleted successfully.
</div>");
            Utility::redirect("index.php");
        }

    }
    public function deleteMultiple($IDs){
        if(count($IDs)>0){
            $ids = implode(",",$IDs);
            $query = "DELETE FROM `booklist` WHERE `id` IN (".$ids.")";
            $result = mysqli_query($this->conn,$query);
            if($result){
                Message::message("Data has been deleted successfully");
                Utility::redirect("index.php");
            }

            else{

                Message::message("Error deleting data");
                Utility::redirect("index.php");
            }
        }
    }


    public function paginator($startFrom=0,$limit=5){
        $hobbies = array();
        $query = "SELECT * FROM `booklist` LIMIT ".$startFrom.",".$limit;
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $hobbies[]=$row;
        return $hobbies;
    }


    public function count(){
        $query = "SELECT COUNT(*) AS total FROM `lms`.`booklist`";
        $result = mysqli_query($this->conn,$query);
        if($row= mysqli_fetch_assoc($result))
            return $row['total'];
    }
    
    
    public function request(){
        $query = "INSERT INTO `requested_books` (`book_id`, `student_id`) VALUES ('".$this->book_id."', '".$this->student_id."')";
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Request has been sent successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }


    public function cancelRequest(){
        $query = "DELETE FROM `requested_books` WHERE `requested_books`.`book_id` ='".$this->book_id."' AND `student_id`='".$this->student_id."'";
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Request has been canceled successfully");
            Utility::redirect("index.php");
        }
        else
            echo "ERROR";
    }


    public function requestedBooks($pageStartFrom=0,$limit=5){
        $allInfo = array();
        $query = "SELECT * FROM booklist RIGHT JOIN requested_books ON `booklist`.`id`=`requested_books`.`book_id` ORDER BY `booklist`.`id`";
        //Utility::d($query);
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_object($result))
            $allInfo[]=$row;
        return $allInfo;
    }


    public function is_requested(){
        $query = "SELECT * FROM `requested_books` WHERE `student_id`='".$this->student_id."' AND `book_id`='".$this->book_id."'";
        $result = mysqli_query($this->conn,$query);
        $requested = mysqli_num_rows($result);
        return $requested;
    }


    public function requestedAmount(){
        $query = "SELECT * FROM `requested_books` WHERE `student_id`=".$this->user_id;
        $result = mysqli_query($this->conn,$query);
        $amount = mysqli_num_rows($result);
        return $amount;
    }


    public function deleteAllRequest(){
        $query = "DELETE FROM `requested_books`";
        $result = mysqli_query($this->conn,$query);
   }


    public function approval(){
        $query = "UPDATE `booklist` SET `amount` = `amount`-1 WHERE `booklist`.`id` = ".$this->book_id;
        $result = mysqli_query($this->conn, $query);
        if(!$result)
            echo "ERROR in approving request";
    }


    public function approveRequest(){
        $query = "DELETE FROM `requested_books` WHERE `requested_books`.`book_id` ='".$this->book_id."' AND `student_id`='".$this->student_id."'";
        $result = mysqli_query($this->conn, $query);
        if($result){
            Message::message("Request has been approved");
            Utility::redirect("../views/Admin/requested_books.php");
        }
        else
            Message::message("ERROR in approving request");
            Utility::redirect("../views/Admin/requested_books.php");
    }


    public function storeBorrow(){
        $date=date("Y-m-d");
        $due_date = date('Y-m-d', strtotime($date. ' + 7 days'));
        $query = "INSERT INTO `borrow` (`book_id`, `student_id`, `borrow_date`, `due_date`, `return_date`) VALUES ('".$this->book_id."', '".$this->student_id."', '".date("Y-m-d")."', '".$due_date."', '')";
        $result = mysqli_query($this->conn, $query);
        if(!$result)
            echo "ERROR";
    }


    public function getBookId(){
        $query = "SELECT `id` FROM `booklist` WHERE `name`='".$this->name."' AND `edition`='".$this->edition."'";
        $result = mysqli_query($this->conn, $query);
        if($row=mysqli_fetch_object($result))
            return $row->id;
    }


    public function increaseBookAmount(){
        $query = "UPDATE `booklist` SET `amount` = `amount`+1 WHERE `booklist`.`id` = ".$this->book_id;
        $result = mysqli_query($this->conn, $query);
        if(!$result)
            echo "ERROR in increasing book amount!";
    }


    public function returnBook(){
        $query1 = "SELECT * FROM `borrow` WHERE `borrow`.`student_id` ='".$this->student_id."' AND `borrow`.`book_id`='".$this->book_id."'";
        $result1 = mysqli_query($this->conn, $query1);
        if($row=mysqli_fetch_object($result1))
            $due_date = $row->due_date;
        $query = "DELETE FROM `borrow` WHERE `borrow`.`student_id` ='".$this->student_id."' AND `borrow`.`book_id`='".$this->book_id."'";
        //Utility::dd($query);
        $result = mysqli_query($this->conn, $query);
        if($result){
            return $due_date;
        }
        else
            echo "ERROR";
    }


    public function rejectRequest(){
        $query = "DELETE FROM `requested_books` WHERE `requested_books`.`request_id` ='".$this->request_id."'";
        //Utility::dd($query);
        $result = mysqli_query($this->conn, $query);
       //Utility::dd($result);
        if($result){
            Message::message("Request has been deleted");
            Utility::redirect("../views/Admin/requested_books.php");
        }
        else
            echo "ERROR";
    }


    public function panel()
    {

        $_allbook = array();
        $query = "SELECT `username` FROM `admin`";
        //Utility::dd($query);
        $result = mysqli_query($this->conn, $query);

        while ($row = mysqli_fetch_object($result)) {
            $_allbook[] = $row;
        }
        return $_allbook;
    }


    public function registerAdmin()
    {
        $query = "INSERT INTO `lms`.`admin` (`username`,`password`) VALUES ('" . $this->name . "','" . $this->password . "')";
        $result = mysqli_query($this->conn, $query);
        //Utility::dd($query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("../../views/Admin/admin_panel.php");
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Fail!</strong> Data has not been stored successfully.
                </div>");
            Utility::redirect("../../views/Admin/admin_register.php");
        }
    }


    public function getAllBookName(){
        $_allBook = array();
        $query = "SELECT `name` FROM `booklist`";
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result))
            $_allBook[]=$row['name'];
        //  Utility::d($_allBook);
        return $_allBook;
    }


    public function getAllSearchData(){
        $_allBook = array();
        $query = "SELECT `name`,`author` FROM `booklist`";
        $result = mysqli_query($this->conn,$query);
        while($row= mysqli_fetch_assoc($result))
            $_allBook[]=$row['name'].'","'.$row['author'];
        //  Utility::d($_allBook);
        return $_allBook;
    }
}



//include_once ('../../views/Admin/requested_books.php')