<?php

session_start();

include_once ('../vendor/autoload.php');
use App\Library\Library;

$obj = new Library();

$_GET['student_id'] = $_SESSION['user_id'];
$obj->prepare($_GET)->request();
