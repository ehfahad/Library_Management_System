<?php

session_start();

include_once('../vendor/autoload.php');

use App\Library\Library;
use App\User\Auth;
use App\Utility\Utility;
use App\Message\Message;


$admin = new \App\Admin\Auth();
$auth = new Auth();

if ((!$auth->logged_in()) && (!$admin->logged_in()))
    Utility::redirect("user_login_signup.php");
if($auth->logged_in())
    $logged_in = true;
else
    $logged_in = false;

if (array_key_exists("itemPerPage", $_SESSION)) {
    if (array_key_exists("itemPerPage", $_GET))
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
} else
    $_SESSION['itemPerPage'] = 5;

$itemPerPage = $_SESSION['itemPerPage'];

$obj = new Library();
$totalItem = $obj->count();
//echo $totalItem;
$totalPage = ceil($totalItem / $itemPerPage);

if (array_key_exists("pageNumber", $_GET))
    $pageNumber = $_GET['pageNumber'];
else
    $pageNumber = 1;

$pagination = "";
for ($count = 1; $count <= $totalPage; $count++) {
    $class = ($pageNumber == $count) ? "active" : "";
    $pagination .= "<li class='$class'><a href='index.php?pageNumber=$count'>$count</a></li>";
}


$pageStartFrom = $itemPerPage * ($pageNumber - 1);

//$allInfo = $obj->paginator($pageStartFrom,$itemPerPage);


if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) {
    $allInfo = $obj->paginator($pageStartFrom, $itemPerPage);
    if($auth->logged_in())
        $amount = $obj->prepare($_SESSION)->requestedAmount();
}
if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET') && isset($_GET['category']))  {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
    if($auth->logged_in())
        $amount = $obj->prepare($_SESSION)->requestedAmount();
}
if ((strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) && isset($_GET['search'])) {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
    if($auth->logged_in())
        $amount = $obj->prepare($_SESSION)->requestedAmount();
}

$availableTitle=$obj->getAllSearchData();
$comma_separated= '"'.implode('","',$availableTitle).'"';
//
//$availableDescription=$obj->getAllDescription();
//$comma_separated2= '"'.implode('","',$availableDescription).'"';
////Utility::dd($comma_separated);

?>

<!DOCTYPE html>
<html>
<head>
    <title>User Index</title>
    <style>
        .toppad5{
            margin-top: 9px;
        }
        .toppad5_2{
            margin-top: 5px;
        }
        .toppad5_3{
            margin-top: 9px;
            color: white;
        }
        .leftpad5 {
            padding-right: 5px;
        }

        a:hover {
            text-decoration: none;
        }
        .toppad5:hover{
            text-decoration: none;
        }
        a:link {
            text-decoration: none;
            color: white;
        }
        footer{
            background-color:#2c3742;
            box-shadow:inset 0px 0px 3px #111;
            color:#fff;
            font-size:14px;
            line-height:25px;
            padding:10px 0px 10px 0px;
            bottom: 0px;
        }

        footer a{
            color:#eee;
            text-decoration:none;
            border-bottom:1px dotted #888;
        }

        footer a:hover{
            color:#aaa;
            text-decoration:none;
            border:0px;
        }

        footer hr{
            margin-top: 10px;
            margin-bottom: 10px;
            border-top: #000 1px solid;
            border-bottom: #212121 1px solid;
        }

        footer .copy{
            font-size:13px;
            margin:15px 0px;
        }
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../Resources/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
    <link href="../Resources/bootstrap/css/style.css" rel="stylesheet" type="text/css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="../Resources/bootstrap/js/bootstrap.min.js"></script>

    <!--  For autocomplete  -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>

</head>
<body>

<div class="container-fluid">


    <div style="margin-bottom: -20px" class="row">


        <nav class=" navbar navbar-inverse">

            <ul class="nav navbar-nav">
                <li><a class="active" href="index.php" >Home</a></li>
                <li><a href="index.php?category=<?php echo "Reference Book" ?>" >Reference Book</a></li>
                <li><a href="index.php?category=<?php echo "Science Fiction" ?>" >Science Fiction</a></li>
                <li><a href="index.php?category=<?php echo "Novel" ?>" >Novel</a></li>
                <li><a href="index.php?category=<?php echo "Physics" ?>" >Physics</a></li>
                <li><a href="index.php?category=<?php echo "Programming" ?>" >Programming</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="toppad5">
                    <form class="form-inline" action="index.php" method="get">
                        <input id="search" class="form-control" placeholder="Search..." type="text" name="search">
                        <button class="btn btn-info" type="submit">Search</button>
                    </form>
                </li>

                <li class="toppad5_2">
                    <form role="form">
                        <div class=" form-group form-inline margintop">
                            <label class="lablewidth" for="sel1"></label>
                            <select class="form-control formwidth" id="sel1" name="itemPerPage">
                                <option <?php if($itemPerPage==5): ?> selected <?php endif;?> >5</option>
                                <option  <?php if($itemPerPage==10): ?> selected <?php endif;?> >10</option>
                                <option <?php if($itemPerPage==15): ?> selected <?php endif;?> >15</option>
                                <option <?php if($itemPerPage==20): ?> selected <?php endif;?> >20</option>
                                <option <?php if($itemPerPage==25): ?> selected <?php endif;?> >25</option>
                            </select>
                            <button class=" btn btn-default buttonwidth" type="submit">Go!</button>

                        </div>
                    </form>
                </li>

                <li>
                    <?php if(!$logged_in) : ?>
                <li><a href="user_login_signup.php">Log In</a></li>
                <?php endif ?>
                <?php if($logged_in) : ?>
                    <li><button class="btn-default btn toppad5_3"><a   href="Authentication/user_logout.php"><font color="red">Log Out</font> </a></button></li>
                <?php endif ?>
                </li>




            </ul>

        </nav>

    </div>





    <div class="row slidemargin">

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="../Resources/Images/slide_image_1.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>পলান সরকার</h3>
                        <p>বাংলাদেশের বাতিঘর বিস্তীর্ন জনপদ আলোকিত করার দায়িত্ব নিয়েছেন তিনি একাই</p>
                    </div>
                </div>

                <div class="item">
                    <img src="../Resources/Images/slide_image_2.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>হুমায়ূন আজাদ</h3>
                        <p>একজন প্রথাবিরোধী লেখক . মৌলবাদী শক্তির বিরুদ্ধে তিনি ছিলেন এক লড়াকু যোদ্ধা</p>
                    </div>
                </div>

                <div class="item">

                    <img src="../Resources/Images/slide_image_3.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>হুমায়ূন আহমেদ</h3>
                        <p>বাংলা সাহিত্যের অন্যতম জনপ্রিয় লেখক। হিমু, মিসির আলীর মত চরিত্র সৃষ্টি করেছেন তিনি</p>
                    </div>
                </div>

                <div class="item">
                    <img src="../Resources/Images/slide_image_4.jpg" alt="Chania" width="100%" height="350">
                    <div class="carousel-caption">
                        <h3>Ahmed Chafa</h3>
                        <p>Most prominent writer.</p>
                    </div>
                </div>


            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>



    <div class="container-fluid">

        <?php if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])) : ?>
            <div class="alert-info" id="message">
                <h4><center><?php echo Message::message() ?></center></h4>
            </div>
        <?php endif; ?>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Book Name</th>
                    <th>Author Name</th>
                    <th>Edition</th>
                    <th>Category</th>
                    <th>Book Cover</th>
                    <th>Location</th>
                    <th>Availability</th>

                </tr>
                </thead>
                <tbody>
                <?php $sl = 0;
                foreach ($allInfo as $info) {
                    $sl++;

                    if($logged_in){
                        $_GET['student_id']= $_SESSION['user_id'];
                        $_GET['book_id'] = $info->id;
                        // Utility::dd($_GET);
                        $is_requested = $obj->prepare($_GET)->is_requested();
                    }

                    ?>

                    <tr>
                        <td width="5%"> <?php echo $sl+$pageStartFrom ?> </td>
                        <td class="text-justify"> <?php echo $info->name ?></td>
                        <td width="15%"> <?php echo $info->author ?> </td>
                        <td class="text-justify" width="10%"><?php echo $info->edition ?></td>
                        <td class="text-justify" width="10%"><?php echo $info->category ?></td>
                        <td class="text-justify" width="20%"><img src="../Resources/images/<?php echo $info->cover ?>" class="img-responsive"
                                                                  width="100px" height="100px"/></td>
                        <td class="text-justify" width="10%"><?php echo $info->location ?></td>
                        <?php if ($info->amount > 0) : ?>
                        <td width="15%"><a href="" disabled="" class="btn btn-success leftpad5" role="button">Available</a>
                            <?php endif ?>
                            <?php if ($info->amount == 0) : ?>
                        <td width="15%"><a href="" disabled="" class="btn btn-danger leftpad5" role="button">Not Available</a>
                            <?php endif ?>
                            <?php if($logged_in) : ?>
                        <td><a href="book_request.php?book_id=<?php echo $info->id ?>" <?php if($info->amount==0 || $amount>1 || $is_requested) : ?> disabled="" <?php endif; ?> class="btn btn-primary">Borrow</a></td>
                        <td><a href="cancel_request.php?book_id=<?php echo $info->id ?>" class="btn btn-danger" <?php if(!$is_requested):?> disabled="" <?php endif; ?> >Cancel Request</a> </td>
                    <?php endif ?>
                    </tr>

                <?php } ?>


                </tbody>
            </table>
        </div>
        <?php if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET') && empty($_GET['search']) && empty($_GET['category'])) { ?>
            <ul class="pagination">
                <?php if ($pageNumber > 1): ?>
                    <li><a href='index.php?pageNumber=<?php echo $pageNumber - 1 ?>'> Prev </a></li> <?php endif; ?>
                <?php echo $pagination; ?>
                <?php if ($pageNumber < $totalPage): ?>
                    <li><a href='index.php?pageNumber=<?php echo $pageNumber + 1 ?>'> Next </a></li> <?php endif; ?>
            </ul>
        <?php } ?>

    </div>
</div>
<footer>
    <div class="container">

        <div class="copy text-center">
            Copyright 2016 <b><a href="Admin/index.php">Core i5</a></b>
        </div>

    </div>
</footer>
<script>
    $('#message').show().delay(2000).fadeOut();


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>
<script>
    $(function () {
        var availableTags = [
            <?php echo $comma_separated?>
        ];
        $("#search").autocomplete({
            source: availableTags
        });
    });
</script>
<!---->
<!--<script type="text/javascript">-->
<!--    function change() // no ';' here-->
<!--    {-->
<!--        var elem = document.getElementById("button");-->
<!--        if (elem.value=="Close Curtain") elem.value = "Open Curtain";-->
<!--        else elem.value = "Close Curtain";-->
<!--    }-->
<!--</script>-->

</body>
</html>
