<?php
session_start();
include_once('../../vendor/autoload.php');

use App\Admin\User;
use App\Admin\Auth;
use App\Library\Library;
use App\Message\Message;
use App\Utility\Utility;

$object= new Library();
$info=$object->prepare($_POST)->registerAdmin();