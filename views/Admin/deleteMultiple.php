<?php

include_once ("../../vendor/autoload.php");
use App\Library\Library;
use App\Utility\Utility;

$multiple_delete = new Library();
if(array_key_exists("mark",$_POST) && !empty($_POST['mark']))
    $multiple_delete->deleteMultiple($_POST['mark']);
else
    Utility::redirect("index.php");

?>