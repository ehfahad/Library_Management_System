<?php

session_start();

include_once('../../vendor/autoload.php');

use App\Library\Library;
use App\Admin\Auth;
use App\Utility\Utility;
use App\Message\Message;

$auth = new Auth();

if (!$auth->logged_in())
    Utility::redirect("admin_login.php");

$logged_in = true;


if (array_key_exists("itemPerPage", $_SESSION)) {
    if (array_key_exists("itemPerPage", $_GET))
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
} 
else
    $_SESSION['itemPerPage'] = 5;

$itemPerPage = $_SESSION['itemPerPage'];

$obj = new Library();
$totalItem = $obj->count();
//echo $totalItem;
$totalPage = ceil($totalItem / $itemPerPage);

if (array_key_exists("pageNumber", $_GET))
    $pageNumber = $_GET['pageNumber'];
else
    $pageNumber = 1;

$pagination = "";
for ($count = 1; $count <= $totalPage; $count++) {
    $class = ($pageNumber == $count) ? "active" : "";
    $pagination .= "<li class='$class'><a href='requested_books.php?pageNumber=$count'>$count</a></li>";
}


$pageStartFrom = $itemPerPage * ($pageNumber - 1);

//$allInfo = $obj->paginator($pageStartFrom,$itemPerPage);


if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) {
    $allInfo = $obj->requestedBooks();
    //\App\Utility\Utility::dd($allInfo);
}

$to_time = strtotime("05:00:00pm");
$d = strtotime("+4 Hours");
$from_time = strtotime(date("h:i:sa",$d));

if(round(($to_time - $from_time),0)<1){
    $obj->deleteAllRequest();
}

//$availableTitle=$obj->getAllTitle();
//$comma_separated= '"'.implode('","',$availableTitle).'"';
//
//$availableDescription=$obj->getAllDescription();
//$comma_separated2= '"'.implode('","',$availableDescription).'"';
////Utility::dd($comma_separated);

?>




<!DOCTYPE html>
<html>
<head>
    <title>Requested Books</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../../Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="../../Resources/bootstrap/css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>


<body>



<div class="header">
    <div class="container-fluid">


        <div class="row">
            <div class="col-md-4">
                <!-- Logo -->
                <div class="logo">
                    <h1><a href="index.php">Library Management System</a></h1>
                </div>
            </div>
            <div  class="col-md-4">
                <div style="margin-top: 8px" class="row">
                    <div class="col-lg-12">
                        <div>
                            <form style="float: right" class="form-inline" action="index.php" method="get">

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">

                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li><a href="admin_logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="index.php"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
                    <li><a href="forms.php"><i class="glyphicon glyphicon-tasks"></i>  Add Book</a></li>
                    <li><a href="requested_books.php"><i class="glyphicon glyphicon-book"></i>  Requested Book</a></li>
                    <li><a href="issued_book.php"><i class="glyphicon glyphicon-book"></i>  Issued Book</a></li>
                    <li><a href="return_book.php"><i class="glyphicon glyphicon-backward"></i>  Return Book</a></li>
                    <li><a href="admin_panel.php"><i class="glyphicon glyphicon-backward"></i>  Admin Panel</a></li>
                    <li class="submenu">
                        <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Pages
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li><a href="../../index.php">Normal Index</a></li>
                            <li><a href="../index.php">User Index</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

        <?php if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])) : ?>
            <div class="alert-info" id="message">
                <h5><center><?php echo Message::message() ?></center></h5>
            </div>
        <?php endif; ?>

        <div style="min-height: 510px" class="col-md-10">
            <h2><center><i><u>Requested Books</u></i></center></h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Book Name</th>
                        <th>Author Name</th>
                        <th>Edition</th>
                        <th>Location</th>
                        <th>Availability</th>
                        <th>Approve</th>
                        <th>Reject</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $sl=0; foreach($allInfo as $info) { $sl++; ?>

                        <tr>
                            <td width="5%"> <?php echo $sl ?> </td>
                            <td width="5%"> <?php echo $info->name ?></td>
                            <td width="11%"> <?php echo $info->author ?> </td>
                            <td class="text-justify" width="10%"><?php echo $info->edition ?></td>
                            <td class="text-justify" width="10%"><?php echo $info->location ?></td>
                            <?php if($info->amount>0) : ?>
                            <td width="15%"><a href="" disabled="" class="btn btn-success leftpad5" role="button">Available<span class="badge"> <?php echo $info->amount ?> </span></a>
                                <?php endif ?>
                                <?php if($info->amount==0) : ?>
                            <td width="15%"><a href="" disabled="" class="btn btn-danger leftpad5" role="button">Not Available</a>
                                <?php endif ?>
                            <td width="15%"><a href="../approve_book.php?book_id=<?php echo $info->id ?>" <?php if ($info->amount<1) : ?> disabled="" <?php endif; ?>class="btn btn-primary leftpad5" role="button">Approve</a>
                            <td width="15%"><a href="../reject_book.php?request_id=<?php echo $info->request_id ?>" class="btn btn-danger leftpad5" role="button">Reject</a>

                        </tr>

                    <?php } ?>


                    </tbody>
                </table>

            </div>
        </div>


    </div>
</div>
</div>

<footer>
    <div class="container">

        <div class="copy text-center">
            Copyright 2016 <b>Core i5</b>
        </div>

    </div>
</footer>

<script>
    $('#message').show().delay(2000).fadeOut();
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
<script src="../../Resources/bootstrap/js/custom.js"></script>
</body>
</html>