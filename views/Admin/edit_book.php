<?php

session_start();
include_once('../../vendor/autoload.php');
use App\Library\Library;
use App\Message\Message;
use App\Utility\Utility;
use App\Admin\Auth;

$auth = new Auth();

if (!$auth->logged_in())
    Utility::redirect("admin_login.php");

$logged_in = true;
$book= new Library;
$book->prepare($_GET);
$singleItem=$book->view();
//Utility::d($singleItem);
?>
<!DOCTYPE html>

<html lang="en">
<head>
    <title>Edit Book Details</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="../../Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="../../Resources/bootstrap/css/styles.css" rel="stylesheet">

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">


    <link href="../../Resources/bootstrap/css/forms.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <![endif]-->
</head>
<body>












<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <!-- Logo -->
                <div class="logo">
                    <h1><a href="index.php">Library Management System</a></h1>
                </div>
            </div>
            <form action="">
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="input-group form">

                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-md-2">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <b
                                        class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li><a href="admin_logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="index.php"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
                    <li><a href="forms.php"><i class="glyphicon glyphicon-tasks"></i>  Add Book</a></li>
                    <li><a href="requested_books.php"><i class="glyphicon glyphicon-book"></i>  Requested Book</a></li>
                    <li><a href="issued_book.php"><i class="glyphicon glyphicon-book"></i>  Issued Book</a></li>
                    <li><a href="return_book.php"><i class="glyphicon glyphicon-backward"></i>  Return Book</a></li>
                    <li><a href="admin_panel.php"><i class="glyphicon glyphicon-backward"></i>  Admin Panel</a></li>
                    <li class="submenu">
                        <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Pages
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li><a href="../../index.php">Normal Index</a></li>
                            <li><a href="../index.php">User Index</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>


        <div class="col-md-10">


            <div style="min-height: 800px" class="row">
                <div class="col-md-12">
                    <div class="content-box-large">


                        <div class="container">

                            <form role="form" method="post" action="update_book.php" enctype="multipart/form-data">
                                <div class="form-group">

                                    <h2>Edit Book</h2>
                                    <input class="form-control" type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
                                    <label>Book Name:</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter Book Name" value="<?php echo $singleItem->name?>">
                                    <label>Author Name:</label>
                                    <input type="text" name="author" class="form-control" id="author" placeholder="Enter Author Name" value="<?php echo $singleItem->author?>">
                                    <label>Edition:</label>
                                    <input type="text" name="edition" class="form-control" id="edition" placeholder="Enter Edition" value="<?php echo $singleItem->edition?>">
                                    <label>Cover</label>
                                    <input type="file" name="cover" class="form-control" >
                                    <img src="../../Resources/Images/<?php echo $singleItem->cover?>"alt="cover" height="421px" width="270px" class="img-responsive">

                                    <br> <br>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Select Location </label>

                                        <div class="col-md-4">
                                            <select class="form-control" name="location" id="sel1">
                                                <option>Select any</option>
                                                <option <?php if($singleItem->location=="Shelf 1"):?>selected<?php endif ?>>Shelf 1</option>
                                                <option <?php if($singleItem->location=="Shelf 2"):?>selected<?php endif ?>>Shelf 2</option>
                                                <option <?php if($singleItem->location=="Shelf 3"):?>selected<?php endif ?>>Shelf 3</option>
                                                <option <?php if($singleItem->location=="Shelf 4"):?>selected<?php endif ?>>Shelf 4</option>

                                            </select>
                                        </div>


                                        <label class="col-md-2 control-label">Select Category </label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="category" id="sel1">
                                                <option>Select any</option>
                                                <option <?php if($singleItem->category=="Science Fiction"):?>selected<?php endif ?>>Science Fiction</option>
                                                <option <?php if($singleItem->category=="Novel"):?>selected<?php endif ?>>Novel</option>
                                                <option <?php if($singleItem->category=="Physics"):?>selected<?php endif ?>>Physics</option>
                                                <option <?php if($singleItem->category=="Programming"):?>selected<?php endif ?>>Programming</option>
                                                <option <?php if($singleItem->category=="Reference Book"):?>selected<?php endif ?>>Reference Book</option>

                                            </select>
                                        </div>

                                        <label>amount</label>
                                        <input type="number" name="amount" class="form-control" id="amount" placeholder="Enter Number" value="<?php echo $singleItem->amount?>">
                                    </div>

                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a href="index.php" class="btn btn-warning">Cancel</a>
                            </form>
                        </div>


                        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                        <script src="https://code.jquery.com/jquery.js"></script>
                        <!-- jQuery UI -->
                        <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
                        <!-- Include all compiled plugins (below), or include individual files as needed -->
                        <script src="bootstrap/js/bootstrap.min.js"></script>

                        <script src="vendors/form-helpers/js/bootstrap-formhelpers.min.js"></script>

                        <script src="vendors/select/bootstrap-select.min.js"></script>

                        <script src="vendors/tags/js/bootstrap-tags.min.js"></script>

                        <script src="vendors/mask/jquery.maskedinput.min.js"></script>

                        <script src="vendors/moment/moment.min.js"></script>

                        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

                        <!-- bootstrap-datetimepicker -->
                        <link href="vendors/bootstrap-datetimepicker/datetimepicker.css" rel="stylesheet">
                        <script src="vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.js"></script>


                        <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
                              rel="stylesheet"/>
                        <script
                            src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

                        <script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
                        <script src="../../Resources/bootstrap/js/custom.js"></script>
                       </div>
                    </div>

                        <footer>
                            <div class="container">

                                <div class="copy text-center">
                                    Copyright 2016 Core i5 
                                </div>

                            </div>
                        </footer>
</body>
</html>



