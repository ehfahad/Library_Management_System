<?php

include_once ('../../vendor/autoload.php');

use App\Library\Library;
use App\Admin\Auth;
use App\Utility\Utility;

$obj = new Auth();
$obj->prepare($_GET);
$obj->deleteAdmin();
Utility::redirect("admin_panel.php");
