<?php

session_start();

include_once('../../vendor/autoload.php');
use App\Library\Library;
use App\Message\Message;
use App\Utility\Utility;
use App\Admin\Auth;

$auth = new Auth();

//if (!$auth->logged_in())
//  Utility::redirect("admin_login.php");

$logged_in = true;
$book= new Library;
$book->prepare($_GET);
$singleItem=$book->view();
//Utility::d($singleItem);

$auth = new Auth();

if (!$auth->logged_in())
	Utility::redirect("admin_login.php");

$logged_in = true;
if (array_key_exists("itemPerPage", $_SESSION)) {
	if (array_key_exists("itemPerPage", $_GET))
		$_SESSION['itemPerPage'] = $_GET['itemPerPage'];
} else
	$_SESSION['itemPerPage'] = 5;

$itemPerPage = $_SESSION['itemPerPage'];

$obj = new Library();
$totalItem = $obj->count();
//echo $totalItem;
$totalPage = ceil($totalItem / $itemPerPage);

if (array_key_exists("pageNumber", $_GET))
	$pageNumber = $_GET['pageNumber'];
else
	$pageNumber = 1;

$pagination = "";
for ($count = 1; $count <= $totalPage; $count++) {
	$class = ($pageNumber == $count) ? "active" : "";
	$pagination .= "<li class='$class'><a href='index.php?pageNumber=$count'>$count</a></li>";
}


$pageStartFrom = $itemPerPage * ($pageNumber - 1);

//$allInfo = $obj->paginator($pageStartFrom,$itemPerPage);


if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) {
	$allInfo = $obj->paginator($pageStartFrom, $itemPerPage);
}
if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET') && isset($_GET['category']))  {
	$obj->prepare($_GET);
	$allInfo = $obj->index();
}
if ((strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) && isset($_GET['search'])) {
	$obj->prepare($_GET);
	$allInfo = $obj->index();
}

//Utility::dd($itemPerPage);
//$availableTitle=$obj->getAllTitle();
//$comma_separated= '"'.implode('","',$availableTitle).'"';
//
//$availableDescription=$obj->getAllDescription();
//$comma_separated2= '"'.implode('","',$availableDescription).'"';
////Utility::dd($comma_separated);

?>




<!DOCTYPE html>
<html>
  <head>
    <title>Library Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../../Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="../../Resources/bootstrap/css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="header">
	     <div class="container-fluid">


	        <div class="row">
	           <div class="col-md-4">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="index.php">Library Management System</a></h1>
	              </div>
	           </div>
	           <div  class="col-md-4">
	              <div class="row">
	                <div class="col-lg-12">
	                  <div>
						 <form style="float: right" class="form-inline" action="index.php" method="get">
	                       <input  size="50" type="text" class="form-control " name="search" placeholder="Search...">

	                         <button class="btn btn-primary" type="button" >Search</button>
	                       </span>
						  </form>
	                  </div>
	                </div>
	              </div>
	           </div>
	           <div class="col-md-4">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">






						  <ul class="nav navbar-nav">

							  <li class="dropdown">
								  <a href="" class="dropdown-toggle" data-toggle="dropdown">Downloads <b class="caret"></b></a>
								  <ul class="dropdown-menu animated fadeInUp">
									  <li>

										  <a href="pdf.php" class="btn btn-default" role="button">Download As PDF</a>
										  <a href="xl.php" class="btn btn-default" role="button">Download As XL File</a>



									  </li>
								  </ul>
							  </li>
						  </ul>








						  <ul class="nav navbar-nav">

							  <li class="dropdown">
								  <a href="" class="dropdown-toggle" data-toggle="dropdown">Pagination <b class="caret"></b></a>
								  <ul class="dropdown-menu animated fadeInUp">
									  <li>

										  <form role="form">
											  <div class=" form-group form-inline margintop">
												  Item Per Page :
												  <label class="lablewidth" for="sel1"></label>
												  <select class="form-control formwidth" id="sel1" name="itemPerPage">
													  <option <?php if($itemPerPage==5): ?> selected <?php endif;?> >5</option>
													  <option  <?php if($itemPerPage==10): ?> selected <?php endif;?> >10</option>
													  <option <?php if($itemPerPage==15): ?> selected <?php endif;?> >15</option>
													  <option <?php if($itemPerPage==20): ?> selected <?php endif;?> >20</option>
													  <option <?php if($itemPerPage==25): ?> selected <?php endif;?> >25</option>
												  </select>
												  <button class=" btn btn-default buttonwidth" type="submit">Go!</button>

											  </div>
										  </form>



									  </li>
								  </ul>
							  </li>
						  </ul>











	                                 <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="" class="dropdown-toggle" data-toggle="dropdown">My Account <b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          <li><a href="admin_logout.php">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>

	     </div>
	</div>

    <div class="page-content">
    	<div class="row">
		  <div class="col-md-2">
		  	<div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="index.php"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>


					<li><a href="forms.php"><i class="glyphicon glyphicon-tasks"></i>  Add Book</a></li>
                    <li><a href="requested_books.php"><i class="glyphicon glyphicon-book"></i>  Requsted Book</a></li>
					<li><a href="issued_book.php"><i class="glyphicon glyphicon-book"></i>  Issued Book</a></li>
                    <li><a href="return_book.php"><i class="glyphicon glyphicon-backward"></i>  Return Book</a></li>
                    <li><a href="admin_panel.php"><i class="glyphicon glyphicon-backward"></i>  Admin Panel</a></li>

                    <li class="submenu">
                         <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Pages
                            <span class="caret pull-right"></span>
                         </a>
                         <!-- Sub menu -->
                         <ul>
                            <li><a href="../../index.php">Normal Index</a></li>
                            <li><a href="../index.php">User Index</a></li>
                        </ul>
                    </li>
                </ul>
             </div>
		  </div>
		  <div style="min-height: 610px; width: 80%" class="col-md-10">

			  <div class="container">
				  <h2>Edit Book</h2>
				  <form class="form-group " role="form" method="post" action="update_book.php" enctype="multipart/form-data">

					   <div class="form-group">

						  <input class="form-control" type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
						  <label>Book Name:</label>
						  <input type="text" name="name" class="form-control" id="name" placeholder="Enter Book Name" value="<?php echo $singleItem->name?>">
						  <label>Author Name:</label>
						  <input type="text" name="author" class="form-control" id="author" placeholder="Enter Author Name" value="<?php echo $singleItem->author?>">
						  <label>Edition:</label>
						  <input type="text" name="edition" class="form-control" id="edition" placeholder="Enter Edition" value="<?php echo $singleItem->edition?>">
						  <label>Cover</label>
						  <input type="file" name="cover" class="form-control" >
						  <img src="../../Resources/Images/<?php echo $singleItem->cover?>"alt="cover" height="421px" width="270px" class="img-responsive">
						  </div>

						  <div class="form-group">
							  <label class="col-md-2 control-label">Select Location </label>

							  <div class="col-md-4">
								  <select class="form-control" name="location" id="sel1">
									  <option>Select any</option>
									  <option <?php if($singleItem->location=="Shelf 1"):?>selected<?php endif ?>>Shelf 1</option>
									  <option <?php if($singleItem->location=="Shelf 2"):?>selected<?php endif ?>>Shelf 2</option>
									  <option <?php if($singleItem->location=="Shelf 3"):?>selected<?php endif ?>>Shelf 3</option>
									  <option <?php if($singleItem->location=="Shelf 4"):?>selected<?php endif ?>>Shelf 4</option>

								  </select>
							  </div>


							  <label class="col-md-2 control-label">Select Category </label>
							  <div class="col-md-4">
								  <select class="form-control" name="category" id="sel1">
									  <option>Select any</option>
									  <option <?php if($singleItem->category=="Science Fiction"):?>selected<?php endif ?>>Science Fiction</option>
									  <option <?php if($singleItem->category=="Novel"):?>selected<?php endif ?>>Novel</option>
									  <option <?php if($singleItem->category=="Physics"):?>selected<?php endif ?>>Physics</option>
									  <option <?php if($singleItem->category=="Programming"):?>selected<?php endif ?>>Programming</option>
									  <option <?php if($singleItem->category=="Reference Book"):?>selected<?php endif ?>>Reference Book</option>

								  </select>
							  </div>

							  <label>amount</label>
							  <input type="number" name="amount" class="form-control" id="amount" placeholder="Enter Number" value="<?php echo $singleItem->amount?>">
						  </div>

						  <button type="submit" class="btn btn-primary">Update</button>
						  <a href="index.php" class="btn btn-warning">Cancel</a>
				  </form>
			  </div>

			  </div>




		  </div>
		  </div>
		</div>
    </div>



    <footer>
         <div class="container">

            <div class="copy text-center">
               Copyright 2016 <b>Core i5</b>
            </div>

         </div>
      </footer>
	<script>
		$('#message').show().delay(2840).fadeOut();


		//        $(document).ready(function(){
		//            $("#delete").click(function(){
		//                if (!confirm("Do you want to delete")){
		//                    return false;
		//                }
		//            });
		//        });
		function ConfirmDelete()
		{
			var x = confirm("Are you sure you want to delete?");
			if (x)
				return true;
			else
				return false;
		}
		$('#multiple_delete').on('click',function(){
			document.forms[0].action="deleteMultiple.php";
			$('#multiple').submit();
		});



	</script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../Resources/bootstrap/js/custom.js"></script>
  </body>
</html>