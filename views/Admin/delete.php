<?php
include_once ('../../vendor/autoload.php');
use App\Library\Library;
use App\Admin\Auth;
use App\Utility\Utility;

$auth = new Auth();

if (!$auth->logged_in())
    Utility::redirect("admin_login.php");

$logged_in = true;

$profile_picture= new Library();
$single_info=$profile_picture->prepare($_GET)->view();
unlink($_SERVER['DOCUMENT_ROOT'].'/Library/Resources/Images/'.$single_info->cover);
$profile_picture->prepare($_GET)->delete();

