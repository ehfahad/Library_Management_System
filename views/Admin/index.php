<?php

session_start();

include_once('../../vendor/autoload.php');


use App\Library\Library;
use App\Message\Message;
use App\Utility\Utility;
use App\Admin\Auth;

$auth = new Auth();

if (!$auth->logged_in())
    Utility::redirect("admin_login.php");

$logged_in = true;

if (array_key_exists("itemPerPage", $_SESSION)) {
    if (array_key_exists("itemPerPage", $_GET))
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
} else
    $_SESSION['itemPerPage'] = 5;

$itemPerPage = $_SESSION['itemPerPage'];

$obj = new Library();
$totalItem = $obj->count();
//echo $totalItem;
$totalPage = ceil($totalItem / $itemPerPage);

if (array_key_exists("pageNumber", $_GET))
    $pageNumber = $_GET['pageNumber'];
else
    $pageNumber = 1;

$pagination = "";
for ($count = 1; $count <= $totalPage; $count++) {
    $class = ($pageNumber == $count) ? "active" : "";
    $pagination .= "<li class='$class'><a href='index.php?pageNumber=$count'>$count</a></li>";
}


$pageStartFrom = $itemPerPage * ($pageNumber - 1);

//$allInfo = $obj->paginator($pageStartFrom,$itemPerPage);


if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) {
    $allInfo = $obj->paginator($pageStartFrom, $itemPerPage);
}
if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET') && isset($_GET['category'])) {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
}
if ((strtoupper($_SERVER['REQUEST_METHOD'] == 'GET')) && isset($_GET['search'])) {
    $obj->prepare($_GET);
    $allInfo = $obj->index();
}

//Utility::dd($itemPerPage);
$availableTitle = $obj->getAllSearchData();
$comma_separated = '"' . implode('","', $availableTitle) . '"';
//Utility::dd($comma_separated);
//
//$availableDescription=$obj->getAllDescription();
//$comma_separated2= '"'.implode('","',$availableDescription).'"';
////Utility::dd($comma_separated);

?>


<!DOCTYPE html>
<html>
<head>
    <title>Admin Index</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--  For autocomplete  -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <!-- Bootstrap -->
    <link href="../../Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="../../Resources/bootstrap/css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->



</head>
<body>
<div class="header">
    <div class="container-fluid">


        <div class="row">
            <div class="col-md-4">
                <!-- Logo -->
                <div class="logo">
                    <h1><a href="index.php">Library Management System</a></h1>
                </div>
            </div>
            <div class="col-md-4">
                <div style="margin-top: 8px" class="row">
                    <div class="col-lg-12">
                        <div>
                            <form style="float: right" class="form-inline" action="index.php" method="get">
                                <input size="40" id="search" type="text" class="form-control " name="search"
                                       placeholder="Search...">

                                <button class="btn btn-primary" type="button">Search</button>
                                </span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="navbar navbar-inverse" role="banner">
                    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">


                        <ul class="nav navbar-nav">

                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown">Downloads <b
                                        class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li>

                                        <a href="pdf.php" class="btn btn-default" role="button">Download As PDF</a>
                                        <a href="xl.php" class="btn btn-default" role="button">Download As XL File</a>


                                    </li>
                                </ul>
                            </li>
                        </ul>


                        <ul class="nav navbar-nav">

                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown">Pagination <b
                                        class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li>

                                        <form role="form">
                                            <div class=" form-group form-inline margintop">
                                                Item Per Page :
                                                <label class="lablewidth" for="sel1"></label>
                                                <select class="form-control formwidth" id="sel1" name="itemPerPage">
                                                    <option <?php if ($itemPerPage == 5): ?> selected <?php endif; ?> >
                                                        5
                                                    </option>
                                                    <option <?php if ($itemPerPage == 10): ?> selected <?php endif; ?> >
                                                        10
                                                    </option>
                                                    <option <?php if ($itemPerPage == 15): ?> selected <?php endif; ?> >
                                                        15
                                                    </option>
                                                    <option <?php if ($itemPerPage == 20): ?> selected <?php endif; ?> >
                                                        20
                                                    </option>
                                                    <option <?php if ($itemPerPage == 25): ?> selected <?php endif; ?> >
                                                        25
                                                    </option>
                                                </select>
                                                <button class=" btn btn-default buttonwidth" type="submit">Go!</button>

                                            </div>
                                        </form>


                                    </li>
                                </ul>
                            </li>
                        </ul>


                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown">My Account <b
                                        class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp">
                                    <li><a href="admin_logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="page-content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar content-box" style="display: block;">
                <ul class="nav">
                    <!-- Main menu -->
                    <li class="current"><a href="index.php"><i class="glyphicon glyphicon-home"></i> Dashboard</a></li>
                    <li><a href="forms.php"><i class="glyphicon glyphicon-tasks"></i> Add Book</a></li>
                    <li><a href="requested_books.php"><i class="glyphicon glyphicon-book"></i> Requested Book</a></li>
                    <li><a href="issued_book.php"><i class="glyphicon glyphicon-book"></i> Issued Book</a></li>
                    <li><a href="return_book.php"><i class="glyphicon glyphicon-backward"></i> Return Book</a></li>
                    <li><a href="admin_panel.php"><i class="glyphicon glyphicon-backward"></i> Admin Panel</a></li>
                    <li class="submenu">
                        <a href="#">
                            <i class="glyphicon glyphicon-list"></i> Pages
                            <span class="caret pull-right"></span>
                        </a>
                        <!-- Sub menu -->
                        <ul>
                            <li><a href="../../index.php">Normal Index</a></li>
                            <li><a href="../index.php">User Index</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>

        <?php if(array_key_exists('message',$_SESSION) && !empty($_SESSION['message'])) : ?>
            <div class="alert-info" id="message">
                <h4><center><?php echo Message::message() ?></center></h4>
            </div>
        <?php endif; ?>

        <form action="deleteMultiple.php" method="post">
            <button type="submit" class="btn btn-danger">Delete Selected</button>
            <div style="min-height: 800px" class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>Check Item</td>
                        <th>#</th>
                        <th>ID</th>
                        <th>Book Name</th>
                        <th>Author Name</th>
                        <th>Edition</th>
                        <th>Book Cover</th>
                        <th>Location</th>
                        <th>Availability</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $sl = 0;
                    foreach ($allInfo as $info) {
                    $sl++; ?>

                    <tr>

                        <td width="5%"><input type="checkbox" name="mark[]" value="<?php echo $info->id ?>"></td>
        </form>

        <td width="5%"> <?php echo $sl + $pageStartFrom ?> </td>
        <td width="5%"> <?php echo $info->id ?></td>
        <td width="15%"> <?php echo $info->name ?></td>
        <td width="15%"> <?php echo $info->author ?> </td>
        <td width="5%" class="text-justify"><?php echo $info->edition ?></td>
        <td width="10%" class="text-justify"><img src="../../Resources/Images/<?php echo $info->cover ?>"
                                                  class="img-responsive" width="100px" height="100px"/></td>
        <td width="5%" class="text-justify"><?php echo $info->location ?></td>
        <?php if ($info->amount > 0) : ?>
        <td width="10%"><a href="" class="btn btn-success leftpad5" disabled="" role="button">Available<span
                    class="badge"> <?php echo $info->amount ?> </span></a>
            <?php endif ?>
            <?php if ($info->amount == 0) : ?>
        <td><a href="" class="btn btn-danger leftpad5" disabled="" role="button">Not Available</a>
            <?php endif ?>
        <td width="30%"><a href="view.php?id=<?php echo $info->id ?>" class="btn btn-primary" role="button">View</a>
            <a href="edit_book.php?id=<?php echo $info->id ?>" class="btn btn-info" role="button">Edit</a>
            <a href="delete.php?id=<?php echo $info->id ?>" class="btn btn-danger" role="button" id="delete"
               Onclick="return ConfirmDelete()">Delete</a>
        </td>

        </tr>

        <?php } ?>
        </tbody>
        </table>

        <?php if (strtoupper($_SERVER['REQUEST_METHOD'] == 'GET') && empty($_GET['search']) && empty($_GET['category'])) { ?>
            <ul class="pagination">
                <?php if ($pageNumber > 1): ?>
                    <li><a href='index.php?pageNumber=<?php echo $pageNumber - 1 ?>'> Prev </a></li> <?php endif; ?>
                <?php echo $pagination; ?>
                <?php if ($pageNumber < $totalPage): ?>
                    <li><a href='index.php?pageNumber=<?php echo $pageNumber + 1 ?>'> Next </a></li> <?php endif; ?>
            </ul>

        <?php } ?>


    </div>
</div>
</div>
</div>


<footer>
    <div class="container">

        <div class="copy text-center">
            Copyright 2016 <b>Core i5</b>
        </div>

    </div>
</footer>
<script>
    $('#message').show().delay(2000).fadeOut();


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }
////    $('#multiple_delete').on('click', function () {
////        document.forms[0].action = "deleteMultiple.php";
////        $('#multiple').submit();
////    });
//
//
//</script>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../Resources/bootstrap/js/bootstrap.min.js"></script>
<script src="../../Resources/bootstrap/js/custom.js"></script>
</body>
</html>